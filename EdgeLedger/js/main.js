function initMap(){
    const loc={lat: 23.022505, lng: 72.571365};

    const map=new google.maps.Map(document.querySelector('.map'),{
        zoom: 14,
        center: loc    
    });

    const marker = new google.maps.Marker({position: loc, map: map });
}